<?php

class RedirFinder extends Tool {
	
	// tool properties
	protected $toolName = 'RedirFinder';
	protected $hasToolDatabase = false;
	
	// for inputs
	protected $category;
	protected $catdepth = 0;
	protected $projectlang = 'de';
	protected $projectdb = 'dewiki';
	protected $project = 'wikipedia';
	protected $ns0articles = false;
	protected $ns0linked = false;
	protected $showunlinked = false;
	
	// configuration
	protected $available_projects = array('wikipedia' => 'wiki');
	 // list from https://quarry.wmflabs.org/query/12744; ordered alphabetically
	protected $available_languages = array('aa' => 'aawiki', 'ab' => 'abwiki', 'ace' => 'acewiki', 'af' => 'afwiki', 'ak' => 'akwiki', 'am' => 'amwiki', 'an' => 'anwiki', 'ang' => 'angwiki', 'ar' => 'arwiki', 'arc' => 'arcwiki', 'arz' => 'arzwiki', 'as' => 'aswiki', 'ast' => 'astwiki', 'av' => 'avwiki', 'ay' => 'aywiki', 'az' => 'azwiki', 'azb' => 'azbwiki', 'ba' => 'bawiki', 'bar' => 'barwiki', 'bcl' => 'bclwiki', 'be' => 'bewiki', 'be-tarask' => 'be_x_oldwiki', 'bg' => 'bgwiki', 'bho' => 'bhwiki', 'bi' => 'biwiki', 'bjn' => 'bjnwiki', 'bm' => 'bmwiki', 'bn' => 'bnwiki', 'bo' => 'bowiki', 'bpy' => 'bpywiki', 'br' => 'brwiki', 'bs' => 'bswiki', 'bug' => 'bugwiki', 'bxr' => 'bxrwiki', 'ca' => 'cawiki', 'cbk-zam' => 'cbk_zamwiki', 'cdo' => 'cdowiki', 'ce' => 'cewiki', 'ceb' => 'cebwiki', 'ch' => 'chwiki', 'cho' => 'chowiki', 'chr' => 'chrwiki', 'chy' => 'chywiki', 'ckb' => 'ckbwiki', 'co' => 'cowiki', 'cr' => 'crwiki', 'crh-latn' => 'crhwiki', 'cs' => 'cswiki', 'csb' => 'csbwiki', 'cu' => 'cuwiki', 'cv' => 'cvwiki', 'cy' => 'cywiki', 'da' => 'dawiki', 'de' => 'dewiki', 'diq' => 'diqwiki', 'dsb' => 'dsbwiki', 'dv' => 'dvwiki', 'dz' => 'dzwiki', 'ee' => 'eewiki', 'el' => 'elwiki', 'eml' => 'emlwiki', 'en' => 'enwiki', 'en' => 'test2wiki', 'en' => 'testwiki', 'en' => 'simplewiki', 'en' => 'tenwiki', 'en' => 'adywiki', 'en' => 'jamwiki', 'eo' => 'eowiki', 'es' => 'eswiki', 'et' => 'etwiki', 'eu' => 'euwiki', 'ext' => 'extwiki', 'fa' => 'fawiki', 'ff' => 'ffwiki', 'fi' => 'fiwiki', 'fj' => 'fjwiki', 'fo' => 'fowiki', 'fr' => 'frwiki', 'frp' => 'frpwiki', 'frr' => 'frrwiki', 'fur' => 'furwiki', 'fy' => 'fywiki', 'ga' => 'gawiki', 'gag' => 'gagwiki', 'gan' => 'ganwiki', 'gd' => 'gdwiki', 'gl' => 'glwiki', 'glk' => 'glkwiki', 'gn' => 'gnwiki', 'gom' => 'gomwiki', 'got' => 'gotwiki', 'gsw' => 'alswiki', 'gu' => 'guwiki', 'gv' => 'gvwiki', 'ha' => 'hawiki', 'hak' => 'hakwiki', 'haw' => 'hawwiki', 'he' => 'hewiki', 'hi' => 'hiwiki', 'hif' => 'hifwiki', 'ho' => 'howiki', 'hr' => 'hrwiki', 'hsb' => 'hsbwiki', 'ht' => 'htwiki', 'hu' => 'huwiki', 'hy' => 'hywiki', 'hz' => 'hzwiki', 'ia' => 'iawiki', 'id' => 'idwiki', 'ie' => 'iewiki', 'ig' => 'igwiki', 'ii' => 'iiwiki', 'ik' => 'ikwiki', 'ilo' => 'ilowiki', 'io' => 'iowiki', 'is' => 'iswiki', 'it' => 'itwiki', 'iu' => 'iuwiki', 'ja' => 'jawiki', 'jbo' => 'jbowiki', 'jv' => 'jvwiki', 'ka' => 'kawiki', 'kaa' => 'kaawiki', 'kab' => 'kabwiki', 'kbd' => 'kbdwiki', 'kg' => 'kgwiki', 'ki' => 'kiwiki', 'kj' => 'kjwiki', 'kk' => 'kkwiki', 'kl' => 'klwiki', 'km' => 'kmwiki', 'kn' => 'knwiki', 'ko' => 'kowiki', 'koi' => 'koiwiki', 'kr' => 'krwiki', 'krc' => 'krcwiki', 'ks' => 'kswiki', 'ksh' => 'kshwiki', 'ku' => 'kuwiki', 'kv' => 'kvwiki', 'kw' => 'kwwiki', 'ky' => 'kywiki', 'la' => 'lawiki', 'lad' => 'ladwiki', 'lb' => 'lbwiki', 'lbe' => 'lbewiki', 'lez' => 'lezwiki', 'lg' => 'lgwiki', 'li' => 'liwiki', 'lij' => 'lijwiki', 'lmo' => 'lmowiki', 'ln' => 'lnwiki', 'lo' => 'lowiki', 'lrc' => 'lrcwiki', 'lt' => 'ltwiki', 'ltg' => 'ltgwiki', 'lv' => 'lvwiki', 'lzh' => 'zh_classicalwiki', 'mai' => 'maiwiki', 'map-bms' => 'map_bmswiki', 'mdf' => 'mdfwiki', 'mg' => 'mgwiki', 'mh' => 'mhwiki', 'mhr' => 'mhrwiki', 'mi' => 'miwiki', 'min' => 'minwiki', 'mk' => 'mkwiki', 'ml' => 'mlwiki', 'mn' => 'mnwiki', 'mo' => 'mowiki', 'mr' => 'mrwiki', 'mrj' => 'mrjwiki', 'ms' => 'mswiki', 'mt' => 'mtwiki', 'mus' => 'muswiki', 'mwl' => 'mwlwiki', 'my' => 'mywiki', 'myv' => 'myvwiki', 'mzn' => 'mznwiki', 'na' => 'nawiki', 'nah' => 'nahwiki', 'nan' => 'zh_min_nanwiki', 'nap' => 'napwiki', 'nb' => 'nowiki', 'nds' => 'ndswiki', 'nds-nl' => 'nds_nlwiki', 'ne' => 'newiki', 'new' => 'newwiki', 'ng' => 'ngwiki', 'nl' => 'nlwiki', 'nn' => 'nnwiki', 'nov' => 'novwiki', 'nrm' => 'nrmwiki', 'nso' => 'nsowiki', 'nv' => 'nvwiki', 'ny' => 'nywiki', 'oc' => 'ocwiki', 'om' => 'omwiki', 'or' => 'orwiki', 'os' => 'oswiki', 'pa' => 'pawiki', 'pag' => 'pagwiki', 'pam' => 'pamwiki', 'pap' => 'papwiki', 'pcd' => 'pcdwiki', 'pdc' => 'pdcwiki', 'pfl' => 'pflwiki', 'pi' => 'piwiki', 'pih' => 'pihwiki', 'pl' => 'plwiki', 'pms' => 'pmswiki', 'pnb' => 'pnbwiki', 'pnt' => 'pntwiki', 'ps' => 'pswiki', 'pt' => 'ptwiki', 'qu' => 'quwiki', 'rm' => 'rmwiki', 'rmy' => 'rmywiki', 'rn' => 'rnwiki', 'ro' => 'rowiki', 'roa-tara' => 'roa_tarawiki', 'ru' => 'ruwiki', 'rue' => 'ruewiki', 'rup' => 'roa_rupwiki', 'rw' => 'rwwiki', 'sa' => 'sawiki', 'sah' => 'sahwiki', 'sc' => 'scwiki', 'scn' => 'scnwiki', 'sco' => 'scowiki', 'sd' => 'sdwiki', 'se' => 'sewiki', 'sg' => 'sgwiki', 'sgs' => 'bat_smgwiki', 'sh' => 'shwiki', 'si' => 'siwiki', 'sk' => 'skwiki', 'sl' => 'slwiki', 'sm' => 'smwiki', 'sn' => 'snwiki', 'so' => 'sowiki', 'sq' => 'sqwiki', 'sr' => 'srwiki', 'srn' => 'srnwiki', 'ss' => 'sswiki', 'st' => 'stwiki', 'stq' => 'stqwiki', 'su' => 'suwiki', 'sv' => 'svwiki', 'sw' => 'swwiki', 'szl' => 'szlwiki', 'ta' => 'tawiki', 'te' => 'tewiki', 'tet' => 'tetwiki', 'tg' => 'tgwiki', 'th' => 'thwiki', 'ti' => 'tiwiki', 'tk' => 'tkwiki', 'tl' => 'tlwiki', 'tn' => 'tnwiki', 'to' => 'towiki', 'tpi' => 'tpiwiki', 'tr' => 'trwiki', 'ts' => 'tswiki', 'tt' => 'ttwiki', 'tum' => 'tumwiki', 'tw' => 'twwiki', 'ty' => 'tywiki', 'tyv' => 'tyvwiki', 'udm' => 'udmwiki', 'ug' => 'ugwiki', 'uk' => 'ukwiki', 'ur' => 'urwiki', 'uz' => 'uzwiki', 've' => 'vewiki', 'vec' => 'vecwiki', 'vep' => 'vepwiki', 'vi' => 'viwiki', 'vls' => 'vlswiki', 'vo' => 'vowiki', 'vro' => 'fiu_vrowiki', 'wa' => 'wawiki', 'war' => 'warwiki', 'wo' => 'wowiki', 'wuu' => 'wuuwiki', 'xal' => 'xalwiki', 'xh' => 'xhwiki', 'xmf' => 'xmfwiki', 'yi' => 'yiwiki', 'yo' => 'yowiki', 'yue' => 'zh_yuewiki', 'za' => 'zawiki', 'zea' => 'zeawiki', 'zh' => 'zhwiki', 'zu' => 'zuwiki');
	protected $output_formats = array('html', 'json-html', 'json');		// first element is default choice
	
	protected $default_category = '';
	protected $default_catdepth = 5;
	
	// internal use
	protected $pages;
	protected $categories = array();
	
	public function __construct($inputdata){
		parent::__construct($inputdata);

		$this->category = isset($inputdata['category'])?str_replace(' ', '_', $inputdata['category']):$this->default_category;
		$this->catdepth = isset($inputdata['catdepth'])?intval($inputdata['catdepth']):$this->default_catdepth;

		if(isset($inputdata['projectlang']) && in_array($inputdata['projectlang'], array_keys($this->available_languages))){
			$this->projectlang = $inputdata['projectlang'];
			$this->projectdb = $this->available_languages[$inputdata['projectlang']];
		}
		if(isset($inputdata['project']) && in_array($inputdata['project'], $this->available_projects)){
			$this->project = $inputdata['project'];
		}
		//if($this->run === '1' && !isset($inputdata['ns0articles'])){
		if($this->run === '1' && isset($inputdata['ns0articles']) && $inputdata['ns0articles'] === '1'){
			$this->ns0articles = true;
		}
		//if($this->run === '1' && !isset($inputdata['ns0linked'])){
		if($this->run === '1' && isset($inputdata['ns0linked']) && $inputdata['ns0linked'] === '1'){
			$this->ns0linked = true;
		}
		
		if($this->run === '1' && isset($inputdata['showunlinked']) && $inputdata['showunlinked'] === '1'){
			$this->showunlinked = true;
		}
	}
	
	public function execute(){
		$this->makeForm();
		
		if($this->run === '1'){	// only if tool should output a result
			$wikiDB = new ReplicaDatabase($this->projectdb);
			if($wikiDB->getToolDBName() !== null){
				$this->pages = array_values($this->scan_cat($wikiDB, $this->category, $this->catdepth));		// this is a catscan with subcategories
				
				$pgs = array();
				foreach($this->pages as $page){
					$pgs[] = $wikiDB->quote($page['page_title']);	// $page['page_namespace'] is available here
				}
				
					// rd_title				//	title of the page that is redirected to
					// rd_namespace			//	namespace of the page that is redirected to
					// page_title			//	title of the redirecting page (this is the redirect)
					// page_namespace		//	namespace of the redirecting page (this is the redirect)
					// pl_from				//	ID of the page on which a redirect is used
					// pl_from_namespace	//	namespace of the page on which a redirect is used
					// pl_namespace			//	dafuq?!  cf. https://www.mediawiki.org/wiki/Manual:Pagelinks_table#pl_namespace
				

				$query  = 'SELECT rd_title, rd_namespace, page_title, page_namespace FROM redirect INNER JOIN page ON rd_from=page_id WHERE rd_title IN (' . implode(',', $pgs) . ')';
				if($this->ns0articles === true){
					$query .= ' AND rd_namespace=0';	// article that is redirected to is in namespace 0
				}
				$statement = $wikiDB->prepare($query);
				$statement->execute();
				$redirects_results = $statement->fetchAll(PDO::FETCH_ASSOC);
				
				$rd_titles = array();
				foreach($redirects_results as $redirect){
					$rd_titles[] = $wikiDB->quote($redirect['page_title']);
				}
				$pagelinkquery = 'SELECT pl_title, COUNT(pl_from) AS cnt FROM pagelinks WHERE pl_title IN (' . implode(',', $rd_titles) . ')';
				if($this->ns0linked === true){
					$pagelinkquery .= ' AND pl_from_namespace IN (0, 10)';	// linked from namespace 0-article or 10-template
				}
				$pagelinkquery .= ' AND pl_namespace IN (0, 10)';	// this fix massively enhances query performance; does it deteriorate results?! it only allows redirects in NS 0 and 10
				$pagelinkquery .= ' GROUP BY pl_title';
				
				$plstatement = $wikiDB->prepare($pagelinkquery);
				$plstatement->execute();
				$pagelinks = array();
				while(false !== ($row = $plstatement->fetch(PDO::FETCH_ASSOC))){
					$pagelinks[$row['pl_title']] = $row['cnt'];
				}
				$plstatement->closeCursor();
				$plstatement = null;
				
				$redirects = array();
				foreach($redirects_results as $redirect){
					if(isset($pagelinks[$redirect['page_title']]) === true || $this->showunlinked === true){
						$rd_namespace = '';
						if($redirect['rd_namespace'] != '0'){
							$rd_namespace = $this->ns($redirect['rd_namespace'], $this->projectlang) . ':';
						}
						$page_namespace = '';
						if($redirect['page_namespace'] != '0'){
							$page_namespace = $this->ns($redirect['page_namespace'], $this->projectlang) . ':';
						}
						
						if(true !== array_key_exists($rd_namespace . $redirect['rd_title'], $redirects)){
							$redirects[$rd_namespace . $redirect['rd_title']] = array('article' => $rd_namespace . $redirect['rd_title'], 'redirects' => array());
						}
						if(true !== array_key_exists($page_namespace . $redirect['page_title'], $redirects[$rd_namespace . $redirect['rd_title']]['redirects'])){
							$redirects[$rd_namespace . $redirect['rd_title']]['redirects'][$page_namespace . $redirect['page_title']] = array('redirect' => $page_namespace . $redirect['page_title'], 'num' => isset($pagelinks[$redirect['page_title']])?$pagelinks[$redirect['page_title']]:'0');
						}
					}
				}
				
				$statement->closeCursor();
				$statement = null;
				$wikiDB = null;
				$this->toolResult = $redirects;
			}
		}
		return $this->userMsg;
	}
	
	private function scan_cat($wikiDB, $category, $catdepth){
		$pages = array();
		$subcategories = array();
		
		array_push($this->categories, $category);
		
		$query = 'SELECT cl_type, cl_from, page_title, page_namespace FROM categorylinks INNER JOIN page ON cl_from=page_id WHERE cl_to=:where';
		if($this->ns0articles === true){
			$query .= ' AND page_namespace IN (\'0\', \'10\', \'14\')';	// 0-article, 10-template (optional), 14-category
		}
		
		$statement = $wikiDB->prepare($query);
		$statement->bindParam(':where', $category);
		$statement->execute();
		while(false !== ($row = $statement->fetch(PDO::FETCH_ASSOC))){
			if($row['cl_type'] === 'subcat'){
				$subcategories[$row['cl_from']] = $row['page_title'];
			}
			else if($row['cl_type'] === 'page'){
				$pages[$row['cl_from']] = array('page_id' => $row['cl_from'], 'page_title' => $row['page_title'], 'page_namespace' => $row['page_namespace']);
			}
		}
		$statement->closeCursor();
		$statement = null;
		
		if($catdepth > 0){
			foreach($subcategories as $pageid => $subcategory){
				$pages = $pages + $this->scan_cat($wikiDB, $subcategory, $catdepth-1);
			}
		}
		
		return $pages;
	}
	
	private function makeForm(){
		$this->userMsg .= '			<form action="' . $_SERVER['SCRIPT_NAME'] . '" method="post"><table class="form"><tbody>';
		$this->userMsg .= '<tr><td><label for="wiki">Project:</label></td><td><select name="projectlang" id="projectlang" style="width:65px;">';
		foreach(array_keys($this->available_languages) as $projectlang){
			$this->userMsg .= '<option value="' . $projectlang . '"';
			if($projectlang === $this->projectlang){
				$this->userMsg .= ' selected="selected"';
			}
			$this->userMsg .= '>' . $projectlang . '</option>';
		}
		$this->userMsg .= '</select>.<select name="project" id="project" style="width:105px;">';
		foreach($this->available_projects as $project => $short_project){
			$this->userMsg .= '<option value="' . $project . '"';
			if($project === $this->project){
				$this->userMsg .= ' selected="selected"';
			}
			$this->userMsg .= '>' . $project . '</option>';
		}
		$this->userMsg .= '</select>.org</td><td></td></tr>';
		$this->userMsg .= '<tr><td><label for="category">Category:</label></td><td><input type="text" name="category" id="category"' . ($this->category !== null?' value="' . str_replace('_', ' ', $this->category) . '"':'') . '></td><td>without namespace prefix</td></tr>';
		$this->userMsg .= '<tr><td><label for="catdepth">Cat search depth:</label></td><td><input type="text" name="catdepth" id="catdepth" value="' . $this->catdepth . '"></td><td></td></tr>';
		$this->userMsg .= '<tr><td><label for="format">Output format:</label></td><td><select name="format" id="format">';
		foreach($this->output_formats as $format){
			$this->userMsg .= '<option value="' . $format . '"';
			if($format === $this->format){
				$this->userMsg .= ' selected="selected"';
			}
			$this->userMsg .= '>' . $format . '</option>';
		}
		$this->userMsg .= '</select></td><td></td></tr>';
		$this->userMsg .= '<tr><td>Options:</td><td colspan="2">';
		$this->userMsg .= '<input type="checkbox" name="ns0articles" id="ns0articles" value="1"' . ($this->ns0articles === true?' checked="checked"':'') . '> <label for="ns0articles">Limit to redirects to pages in namespace 0 (article)</label><br>';
		$this->userMsg .= '<input type="checkbox" name="ns0linked" id="ns0linked" value="1"' . ($this->ns0linked === true?' checked="checked"':'') . '> <label for="ns0linked">Limit to redirects backlinked from namespace 0 (article) or 10 (template)</label><br>';
		$this->userMsg .= '<input type="checkbox" name="showunlinked" id="showunlinked" value="1"' . ($this->showunlinked === true?' checked="checked"':'') . '> <label for="showunlinked">Show unlinked redirects</label>';
		$this->userMsg .= '</td></tr>';
		$this->userMsg .= '<tr><td><input type="hidden" name="run" value="1">';
		$this->userMsg .= '<input type="hidden" name="tool" value="' . $this->toolName . '">';
		$this->userMsg .= '</td><td><input type="submit" value="run"></td><td>';
		if($this->run === '1'){
			$this->userMsg .= 'permanent links of query: ';
			$this->userMsg .= '<a href="' . $_SERVER['SCRIPT_NAME'] . '?tool=' . $this->toolName . '&amp;format=' . $this->format . '&amp;run=1&amp;projectlang=' . $this->projectlang . '&amp;project=' . $this->project . '&amp;category=' . $this->category . '&amp;catdepth=' . $this->catdepth . ($this->ns0articles===true?'&amp;ns0articles=1':'') . ($this->ns0linked===true?'&amp;ns0linked=1':'') . ($this->showunlinked===true?'&amp;showunlinked=1':'') . '">auto run</a>';
			$this->userMsg .= ' • ';
			$this->userMsg .= '<a href="' . $_SERVER['SCRIPT_NAME'] . '?tool=' . $this->toolName . '&amp;format=' . $this->format . '&amp;run=0&amp;projectlang=' . $this->projectlang . '&amp;project=' . $this->project . '&amp;category=' . $this->category . '&amp;catdepth=' . $this->catdepth . ($this->ns0articles===true?'&amp;ns0articles=1':'') . ($this->ns0linked===true?'&amp;ns0linked=1':'') . ($this->showunlinked===true?'&amp;showunlinked=1':''). '">manual run</a>';
		}
		$this->userMsg .= '</td></tr></tbody></table></form>' . N;
	}
	
	public function format_result(){
		if(count($this->toolResult) > 0){
			if(substr($this->format, 0, 4) === 'json'){
				$this->formattedToolResult = json_encode($this->toolResult);
			}
			else if(substr($this->format, 0, 4) === 'html'){
				$this->formattedToolResult .= '			<p>Display ' . count($this->toolResult) . ' results from ' . count($this->pages) . ' pages in ' . count($this->categories) . ' categor' . (count($this->categories) > 1?'ies':'y') . '.';
				if($this->ns0articles === true || $this->ns0linked === true){
					$this->formattedToolResult .= ' Skipped from results:</p>' . N;
					$this->formattedToolResult .= '			<ul>' . N;
					if($this->ns0articles === true){
						$this->formattedToolResult .= '				<li>pages not in namespace 0 (article)</li>' . N;
					}
					if($this->ns0linked === true){
						$this->formattedToolResult .= '				<li>redirects without any backlinks from namespace 0 (article) or 10 (template)</li>' . N;
					}
					if($this->showunlinked === false){
						$this->formattedToolResult .= '				<li>unlinked redirects</li>' . N;
					}
					$this->formattedToolResult .= '			</ul>' . N;
				}
				else {
					$this->formattedToolResult .= '</p>' . N;
				}
				
				
				$this->formattedToolResult .= '			<p>List of results:</p>' . N;
				$this->formattedToolResult .= '			<table class="results">' . N;
				$this->formattedToolResult .= '				<thead>' . N;
				$this->formattedToolResult .= '					<tr><th>#</th><th>target article</th><th>redirects (#' . ($this->ns0linked === true?' from namespace 0 or 10':'') . ')</th></tr>' . N;
				$this->formattedToolResult .= '				</thead>' . N;
				$this->formattedToolResult .= '				<tbody>' . N;
				$i = 1;
				foreach($this->toolResult as $key => $row){
					$outarray = array();
					foreach($row['redirects'] as $key2 => $row2){
						$outarray[] = '<a href="//' . $this->projectlang . '.' . $this->project . '.org/w/index.php?title=' . urlencode($row2['redirect']) . '&amp;redirect=no">' . str_replace('_', ' ', $row2['redirect']) . '</a> (<a href="//' . $this->projectlang . '.' . $this->project . '.org/w/index.php?title=Special:WhatLinksHere&amp;target=' . urlencode($row2['redirect']) . '&amp;namespace=">' . $row2['num'] . ' backlink' . ($row2['num']==1?'':'s') . '</a>)';
					}
					if($this->showunlinked === true || count($outarray) > 0){
						$out = implode('<br>', $outarray);
						$this->formattedToolResult .= '					<tr><td>' . $i . '</td><td><a href="//' . $this->projectlang . '.' . $this->project . '.org/wiki/' . urlencode($row['article']) . '">' . str_replace('_', ' ', $row['article']) . '</a></td><td>' . $out . '</td></tr>' . N;
					}
					$i++;
				}
				$this->formattedToolResult .= '				</tbody>' . N;
				$this->formattedToolResult .= '			</table>' . N;
				
				$this->formattedToolResult .= '<!--' . N;
				$this->formattedToolResult .= print_r($this->toolResult, true);
				$this->formattedToolResult .= '-->' . N;
				
/*				$this->formattedToolResult .= '<!-- full list of pages: ' . N;
				foreach($this->pages as $page){
					$this->formattedToolResult .= $page['page_title'] . ($page['page_namespace'] !== '0'?' (NS ' . $page['page_namespace'] . ')':'') . ', ' . N;
				}
				$this->formattedToolResult .= '-->' . N;
*/			}
		}
		else {	// empty result
			$this->formattedToolResult = null;
		}
		return $this->formattedToolResult;
	}
	
	public static function initDatabase($database){
		return '';
	}
}

?>