<?php

class MainAuthors extends Tool {
	
	// tool properties
	protected $toolName = 'MainAuthors';
	protected $hasToolDatabase = false;
	
	// for inputs
	protected $category;
	protected $catdepth = 0;
	protected $projectlang = 'de';
	protected $project = 'wikipedia';
	
	protected $pagesortkey = 'page_title';
	protected $usersortkey = 'user_name';
	
	protected $pagesortdir = 'asc';
	protected $usersortdir = 'asc';
	
	// configuration
	protected $available_projects = array('wikipedia' => 'wiki', 'wikinews' => 'wikinews');
	protected $available_languages = array('be', 'bg', 'ca', 'cs', 'da', 'de', 'el', 'en', 'es', 'et', 'eu', 'fa', 'fi', 'fo', 'fr', 'gl', 'he', 'hr', 'hu', 'it', 'ja', 'lt', 'lv', 'nl', 'no', 'pl', 'pt', 'ro', 'ru', 'sh', 'sk', 'sl', 'sr', 'sv', 'tr', 'uk', 'zh');
	protected $output_formats = array('html', 'json-html', 'json');		// first element is default choice
	
	protected $default_category = '';
	protected $default_catdepth = 5;
	
	protected $default_limit_users = 100;
	protected $limit_display_users = 0;
	protected $default_detailed_user = '';
	protected $detailed_user = '';
	
	protected $pages_sortkeys = array('page_title' => 'Page title', 'timestamp' => 'Creation timestamp', 'revisions' => 'Number of revisions', 'creator' => 'Creator', 'mosteditoredits' => 'Number of edits by most editor');
	protected $users_sortkeys = array('user_name' => 'User name', 'first' => 'First edit', 'last' => 'Last edit', 'creatoredits' => 'Number of creations', 'mosteditor' => 'Times most editor', 'totaledits' => 'Total edits');
	
	protected $sortdirs = array('asc' => 'Ascending', 'desc' => 'Descending');
	
	protected $permalink_auto = '';
	protected $permalink_manual = '';
	
	// internal use
	protected $categories = array();
	
	public function __construct($inputdata){
		parent::__construct($inputdata);

		$this->category = isset($inputdata['category'])?str_replace(' ', '_', $inputdata['category']):$this->default_category;
		$this->catdepth = isset($inputdata['catdepth'])?intval($inputdata['catdepth']):$this->default_catdepth;

		if(isset($inputdata['projectlang']) && in_array($inputdata['projectlang'], $this->available_languages)){
			$this->projectlang = $inputdata['projectlang'];
		}
		if(isset($inputdata['project']) && in_array($inputdata['project'], $this->available_projects)){
			$this->project = $inputdata['project'];
		}
		
		if(isset($inputdata['pagesortkey']) && array_key_exists($inputdata['pagesortkey'], $this->pages_sortkeys)){
			$this->pagesortkey = $inputdata['pagesortkey'];
		}
		if(isset($inputdata['usersortkey']) && array_key_exists($inputdata['usersortkey'], $this->users_sortkeys)){
			$this->usersortkey = $inputdata['usersortkey'];
		}
		
		if(isset($inputdata['pagesortdir']) && array_key_exists($inputdata['pagesortdir'], $this->sortdirs)){
			$this->pagesortdir = $inputdata['pagesortdir'];
		}
		if(isset($inputdata['usersortdir']) && array_key_exists($inputdata['usersortdir'], $this->sortdirs)){
			$this->usersortdir = $inputdata['usersortdir'];
		}
		
		$this->limit_display_users = isset($inputdata['limitusers'])?intval($inputdata['limitusers']):$this->default_limit_users;
		$this->detailed_user = isset($inputdata['detaileduser'])?str_replace(' ', '_', $inputdata['detaileduser']):$this->default_detailed_user;
		
		$this->permalink_auto = $_SERVER['SCRIPT_NAME'] . '?tool=' . $this->toolName . '&amp;format=' . $this->format . '&amp;run=1&amp;projectlang=' . $this->projectlang . '&amp;project=' . $this->project . '&amp;category=' . $this->category . '&amp;catdepth=' . $this->catdepth . '&amp;detaileduser=' . $this->detailed_user . '&amp;pagesortkey=' . $this->pagesortkey . '&amp;pagesortdir=' . $this->pagesortdir . '&amp;usersortkey=' . $this->usersortkey . '&amp;usersortdir=' . $this->usersortdir . '&amp;limitusers=' . $this->limit_display_users;
		
		$this->permalink_manual = $_SERVER['SCRIPT_NAME'] . '?tool=' . $this->toolName . '&amp;format=' . $this->format . '&amp;run=0&amp;projectlang=' . $this->projectlang . '&amp;project=' . $this->project . '&amp;category=' . $this->category . '&amp;catdepth=' . $this->catdepth . '&amp;detaileduser=' . $this->detailed_user . '&amp;pagesortkey=' . $this->pagesortkey . '&amp;pagesortdir=' . $this->pagesortdir . '&amp;usersortkey=' . $this->usersortkey . '&amp;usersortdir=' . $this->usersortdir . '&amp;limitusers=' . $this->limit_display_users;
	}
	
	public function execute(){
		$this->makeForm();
		
		if($this->run === '1'){	// only if tool should output a result
		
			$replica = new ReplicaDatabase($this->projectlang . $this->available_projects[$this->project]);
			if($replica->getToolDBName() !== null){
				$pages = array_values($this->scan_cat($replica, $this->category, $this->catdepth));		// this is a catscan with subcategories
				
				$stat = array('categories' => count(array_unique($this->categories)), 'catscan_pages' => count($pages), 'processed_pages' => 0, 'revisions' => 0, 'users' => 0);
				
				$pgs = array();
				foreach($pages as $page){
					$pgs[] = $replica->quote($page['page_id']);
				}

				// use table 'revision_userindex' instead of 'revision' due to https://wikitech.wikimedia.org/wiki/Help:MySQL_queries#Alternative_Views
				$query = 'SELECT rev_id, rev_timestamp, actor_user, actor_name, page_id, page_title FROM revision_userindex JOIN page ON rev_page=page_id JOIN actor_revision ON actor_id=rev_actor WHERE page_id IN (' . implode(',', $pgs) . ') ORDER BY page_id ASC, rev_id ASC';

				$statement = $replica->prepare($query);
				$statement->execute();
				
				$pages = array();	//	"page_id" => array("page_id", "page_title", "timestamp", "revisions", "creator", "creatoredits", "mosteditor", "mosteditoredits")
				$users = array();	//	"user_id" => array("user_id", "user_name", "creatoredits", "mosteditor", "totaledits", "first", "last", "page_ids" => array())
				$revisions = array();	// "rev_id" => array("rev_id", "timestamp", "actor_user", "actor_name")
				
				while(false !== ($row = $statement->fetch(PDO::FETCH_ASSOC))){
					$stat['revisions'] = $stat['revisions']+1;
					
					$page_users = array();	// built on every single cycle
					
					if(false === array_key_exists($row['actor_name'], $users)){
						$users[$row['actor_name']] = array('user_id' => $row['actor_user'], 'user_name' => $row['actor_name'], 'creatoredits' => 0, 'mosteditor' => 0, 'totaledits' => 0, 'created' => array(), 'mostedited' => array(), 'first' => 21000000000000, 'last' => 0, 'page_ids' => array(), 'editedpages' => 0);
					}
					if(false === array_key_exists($row['page_id'], $pages)){
						$stat['processed_pages'] = $stat['processed_pages']+1;
						$pages[$row['page_id']] = array('page_id' => $row['page_id'], 'page_title' => $row['page_title'], 'timestamp' => $row['rev_timestamp'], 'revisions' => 0, 'creator' => $row['actor_name'], 'creatoredits' => 0, 'mosteditor' => '', 'mosteditoredits' => 0);
						$revisions = array();	// reset for a new page; pages come ordered with asc page_id
						$users[$pages[$row['page_id']]['creator']]['created'][] = $row['page_title'];
					}
					$users[$row['actor_name']]['page_ids'][] = $row['page_id'];
					
					$users[$row['actor_name']]['totaledits'] = $users[$row['actor_name']]['totaledits']+1;
					
					$users[$row['actor_name']]['first'] = min($row['rev_timestamp'], $users[$row['actor_name']]['first']);
					$users[$row['actor_name']]['last'] = max($row['rev_timestamp'], $users[$row['actor_name']]['last']);
						
					$revisions[$row['rev_id']] = array('rev_id' => $row['rev_id'], 'timestamp' => $row['rev_timestamp'], 'actor_user' => $row['actor_user'], 'actor_name' => $row['actor_name']);
					
					foreach($revisions as $id => $data){
						if(false === array_key_exists($data['actor_name'], $page_users)){
							$page_users[$data['actor_name']] = array('user_id' => $data['actor_user'], 'user_name' => $data['actor_name'], 'local_edits' => 0);
						}
						$page_users[$data['actor_name']]['local_edits'] = $page_users[$data['actor_name']]['local_edits']+1;
					}
					
					$max_edits_in_page = 0;
					foreach($page_users as $user_id => $data){
						$max_edits_in_page = max($data['local_edits'], $max_edits_in_page);
					}
					
					$pages[$row['page_id']]['revisions'] = count($revisions);
					$pages[$row['page_id']]['mosteditoredits'] = $max_edits_in_page;
					
					$pages[$row['page_id']]['creatoredits'] = $page_users[$pages[$row['page_id']]['creator']]['local_edits'];
					
					$most_editor = array();
					foreach($page_users as $user_id => $data){
						if($data['local_edits'] === $max_edits_in_page){
							$most_editor[] = $data['user_name'];
							$users[$data['user_name']]['mostedited'][] = $row['page_title'];
							
						}
					}
					
					$pages[$row['page_id']]['mosteditor'] = $most_editor;	// array!
				}
				
				foreach($users as $user_name => $user){
					$users[$user_name]['created'] = array_unique($users[$user_name]['created']);
					$users[$user_name]['creatoredits'] = count($users[$user_name]['created']);
					
					$users[$user_name]['mostedited'] = array_unique($users[$user_name]['mostedited']);
					$users[$user_name]['mosteditor'] = count($users[$user_name]['mostedited']);
					
					$users[$user_name]['page_ids'] = array_unique($users[$user_name]['page_ids']);
					$users[$user_name]['editedpages'] = count($users[$user_name]['page_ids']);
					
				}
				
				$stat['users'] = count($users);
				
				
				$this->toolResult = array('links' => array('perma_auto' => $this->permalink_auto, 'perma_manual' => $this->permalink_manual), 'stat' => $stat, 'pages' => $pages, 'users' => $users);
				
				$statement->closeCursor();
				$statement=null;
				
				$replica = null;
			}
		}
		return $this->userMsg;
	}
	
	public function format_result(){
		if(count($this->toolResult) > 0){
			if(substr($this->format, 0, 4) === 'json'){
				$this->formattedToolResult = json_encode($this->toolResult);
			}
			else if(substr($this->format, 0, 4) === 'html'){
				$this->formattedToolResult .= '<h3><a name="links" class="anchorlink">Permanent links of query</a></h3>' . N;
				$this->formattedToolResult .= '<p class="anchors"><a href="#top">Top</a> • <a href="#links">Links</a> • <a href="#statistics">Statistics</a> • <a href="#pages">Pages</a> • <a href="#users">Users</a></p>' . N;
				$this->formattedToolResult .= '<p><a href="' . $this->toolResult['links']['perma_auto'] . '">auto run</a> • <a href="' . $this->toolResult['links']['perma_manual'] . '">manual run</a></p>' . N;
				$this->formattedToolResult .= '<h3><a name="statistics" class="anchorlink">Tool result statistics</a></h3>' . N;
				$this->formattedToolResult .= '<p class="anchors"><a href="#top">Top</a> • <a href="#links">Links</a> • <a href="#statistics">Statistics</a> • <a href="#pages">Pages</a> • <a href="#users">Users</a></p>' . N;
				$this->formattedToolResult .= '<ul>' . N;
				$this->formattedToolResult .= '<li>Crawled categories: ' . $this->toolResult['stat']['categories'] . '</li>' . N;
				$this->formattedToolResult .= '<li>Found pages: ' . $this->toolResult['stat']['catscan_pages'] . '</li>' . N;
				$this->formattedToolResult .= '<li>Number of page revisions: ' . $this->toolResult['stat']['revisions'] . '</li>' . N;
				$this->formattedToolResult .= '<li>Involved users: ' . $this->toolResult['stat']['users'] . '</li>' . N;
				$this->formattedToolResult .= '</ul>' . N;
				
				if($this->detailed_user === $this->default_detailed_user){	// normal mode for all users
					$this->formattedToolResult .= '<h3><a name="pages" class="anchorlink">Pages</a></h3>' . N;
					$this->formattedToolResult .= '<p class="anchors"><a href="#top">Top</a> • <a href="#links">Links</a> • <a href="#statistics">Statistics</a> • <a href="#pages">Pages</a> • <a href="#users">Users</a></p>' . N;
					$this->formattedToolResult .= '<style tyle="text/css"> body { max-width:1400px; }</style>' . N;
					$this->formattedToolResult .= '<table class="results">' . N;
					$this->formattedToolResult .= '<tr><th>#</th><th>Article</th><th>Creation timestamp</th><th>Revs</th><th>Creator (edits in article)</th><th>Most edits per user in article</th><th>Most edits by users</th></tr>' . N;
					$orderarray = array();
	//				$orderkey = 'page_title';
					foreach($this->toolResult['pages'] as $page_id => $page){
						$orderarray[$page_id] = $page[$this->pagesortkey];
					}
					if($this->pagesortdir === 'asc'){
						asort($orderarray);
					}
					else if($this->pagesortdir === 'desc'){
						arsort($orderarray);
					}
					
					$cnt = 0;
					foreach($orderarray as $page_id => $ordervalue){
						$cnt++;
						$this->formattedToolResult .= '<tr>';
						$this->formattedToolResult .= '<td>' . $cnt . '</td>';
						$this->formattedToolResult .= '<td><a href="//' . $this->projectlang . '.' . $this->project . '.org/wiki/' . urlencode($this->toolResult['pages'][$page_id]['page_title']) . '" title="' . str_replace('_', ' ', $this->toolResult['pages'][$page_id]['page_title']) . '">' . str_replace('_', ' ', $this->toolResult['pages'][$page_id]['page_title']) . '</a></td>';
						$this->formattedToolResult .= '<td>' . preg_replace('/(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)/', '\3.\2.\1,&nbsp;\4:\5', $this->toolResult['pages'][$page_id]['timestamp']) . '</td>';
						$this->formattedToolResult .= '<td><a href="//' . $this->projectlang . '.' . $this->project . '.org/w/index.php?title=' . urlencode($this->toolResult['pages'][$page_id]['page_title']) . '&amp;action=history" title="history">' . $this->toolResult['pages'][$page_id]['revisions'] . '</a></td>';
						$this->formattedToolResult .= '<td><a href="//' . $this->projectlang . '.' . $this->project . '.org/wiki/User:' . urlencode(str_replace(' ', '_', $this->toolResult['pages'][$page_id]['creator'])) . '" title="User:' . $this->toolResult['pages'][$page_id]['creator'] . '">' . $this->toolResult['pages'][$page_id]['creator'] . '</a> (' . $this->toolResult['pages'][$page_id]['creatoredits'] . ')</td>';
						$this->formattedToolResult .= '<td>' . $this->toolResult['pages'][$page_id]['mosteditoredits'] . '</td>';
						$this->formattedToolResult .= '<td>';
						$formattedEditor = array();
						foreach($this->toolResult['pages'][$page_id]['mosteditor'] as $editor){
							$formattedEditor[] = '<a href="//' . $this->projectlang . '.' . $this->project . '.org/wiki/User:' . urlencode(str_replace(' ', '_', $editor)) . '" title="User:' . $editor . '">' . $editor . '</a>';
						}
						$this->formattedToolResult .= implode('<br>', $formattedEditor);
						$this->formattedToolResult .= '</td>';
						$this->formattedToolResult .= '</tr>' . N;
					}
					$this->formattedToolResult .= '</table>' . N;
					
					$this->formattedToolResult .= '<h3><a name="users" class="anchorlink">Users</a></h3>' . N;
					$this->formattedToolResult .= '<p class="anchors"><a href="#top">Top</a> • <a href="#links">Links</a> • <a href="#statistics">Statistics</a> • <a href="#pages">Pages</a> • <a href="#users">Users</a></p>' . N;
					$this->formattedToolResult .= '<table class="results">' . N;
					$this->formattedToolResult .= '<tr><th>#</th><th>User</th><th>Talk</th><th>Contrib.</th><th>Details</th><th>First edit</th><th>Last edit</th><th colspan="2">Creations</th><th colspan="2">Made most edits<br>in … pages</th><th colspan="2">Edited … pages</th><th colspan="2">Total edits</th></tr>' . N;
					
					$orderarray = array();
	//				$orderkey = 'totaledits';	// creatoredits, mosteditor, totaledits
					foreach($this->toolResult['users'] as $user_name => $user){
						$orderarray[$user_name] = $user[$this->usersortkey];
					}
					if($this->usersortdir === 'asc'){
						asort($orderarray);
					}
					else if($this->usersortdir === 'desc'){
						arsort($orderarray);
					}
					
					$cnt = 0;
					foreach($orderarray as $user_name => $ordervalue){
						$cnt++;
						$this->formattedToolResult .= '<tr>';
						$this->formattedToolResult .= '<td>' . $cnt . '</td>';
						$this->formattedToolResult .= '<td><a href="//' . $this->projectlang . '.' . $this->project . '.org/wiki/User:' . urlencode(str_replace(' ', '_', $this->toolResult['users'][$user_name]['user_name'])) . '" title="User:' . $this->toolResult['users'][$user_name]['user_name'] . '">' . $this->toolResult['users'][$user_name]['user_name'] . '</a></td>';
						$this->formattedToolResult .= '<td><a href="//' . $this->projectlang . '.' . $this->project . '.org/wiki/User_talk:' . urlencode(str_replace(' ', '_', $this->toolResult['users'][$user_name]['user_name'])) . '" title="User talk:' . $this->toolResult['users'][$user_name]['user_name'] . '">talk</a></td>';
						$this->formattedToolResult .= '<td><a href="//' . $this->projectlang . '.' . $this->project . '.org/wiki/Special:Contributions/' . urlencode(str_replace(' ', '_', $this->toolResult['users'][$user_name]['user_name'])) . '" title="user contributions of ' . $this->toolResult['users'][$user_name]['user_name'] . '">contribs</a></td>';
						$this->formattedToolResult .= '<td><a href="' . $_SERVER['SCRIPT_NAME'] . '?tool=' . $this->toolName . '&amp;format=' . $this->format . '&amp;run=1&amp;projectlang=' . $this->projectlang . '&amp;project=' . $this->project . '&amp;category=' . $this->category . '&amp;catdepth=' . $this->catdepth . '&amp;detaileduser=' . $this->toolResult['users'][$user_name]['user_name'] . '&amp;pagesortkey=' . $this->pagesortkey . '&amp;pagesortdir=' . $this->pagesortdir . '&amp;usersortkey=' . $this->usersortkey . '&amp;usersortdir=' . $this->usersortdir . '&amp;limitusers=' . $this->limit_display_users . '">details</a></td>';
						$this->formattedToolResult .= '<td>' . preg_replace('/(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)/', '\3.\2.\1', $this->toolResult['users'][$user_name]['first']) . '</td>';
						$this->formattedToolResult .= '<td>' . preg_replace('/(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)/', '\3.\2.\1', $this->toolResult['users'][$user_name]['last']) . '</td>';
						$this->formattedToolResult .= '<td>' . $this->toolResult['users'][$user_name]['creatoredits'] . '</td><td><small>(' . bcmul(bcdiv($this->toolResult['users'][$user_name]['creatoredits'], $this->toolResult['stat']['catscan_pages'], 3), 100, 1) . '%)</small></td>';
						$this->formattedToolResult .= '<td>' . $this->toolResult['users'][$user_name]['mosteditor'] . '</td><td><small>(' . bcmul(bcdiv($this->toolResult['users'][$user_name]['mosteditor'], $this->toolResult['stat']['catscan_pages'], 3), 100, 1) . '%)</small></td>';
						$this->formattedToolResult .= '<td>' . $this->toolResult['users'][$user_name]['editedpages'] . '</td><td><small>(' . bcmul(bcdiv($this->toolResult['users'][$user_name]['editedpages'], $this->toolResult['stat']['catscan_pages'], 3), 100, 1) . '%)</small></td>';
						$this->formattedToolResult .= '<td>' . $this->toolResult['users'][$user_name]['totaledits'] . '</td><td><small>(' . bcmul(bcdiv($this->toolResult['users'][$user_name]['totaledits'], $this->toolResult['stat']['revisions'], 3), 100, 1) . '%)</small></td>';
						$this->formattedToolResult .= '</tr>' . N;
						if($cnt >= $this->limit_display_users){
							break;
						}
					}
					$this->formattedToolResult .= '</table>' . N;
				}
				else if($this->detailed_user !== $this->default_detailed_user){	// mode for a selected user
					$this->formattedToolResult .= '<p>To be developed</p>' . N;
					$this->formattedToolResult .= '<ul>' . N;
					$this->formattedToolResult .= '<li>User overview</li>' . N;
					$this->formattedToolResult .= '<li>Monthly activity histogram</li>' . N;
					$this->formattedToolResult .= '<li>Pages created by user</li>' . N;
					$this->formattedToolResult .= '<li>Pages user made most edits on</li>' . N;
					$this->formattedToolResult .= '<li>Pages user made edits on</li>' . N;
					$this->formattedToolResult .= '<li>List of edits</li>' . N;
					$this->formattedToolResult .= '</ul>' . N;
				}
			}
		}
		else {	// empty result
			$this->formattedToolResult = null;
		}
		return $this->formattedToolResult;
	}
	
	private function scan_cat($wikiDB, $category, $catdepth){
		$pages = array();
		$subcategories = array();
		
		array_push($this->categories, $category);
		
		$query = 'SELECT cl_type, cl_from, page_title, page_namespace FROM categorylinks INNER JOIN page ON cl_from=page_id WHERE cl_to=:where AND page_namespace IN (\'0\', \'14\')';
		
		$statement = $wikiDB->prepare($query);
		$statement->bindParam(':where', $category);
		$statement->execute();
		while(false !== ($row = $statement->fetch(PDO::FETCH_ASSOC))){
			if($row['cl_type'] === 'subcat'){
				$subcategories[$row['cl_from']] = $row['page_title'];
			}
			else if($row['cl_type'] === 'page'){
				$pages[$row['cl_from']] = array('page_id' => $row['cl_from'], 'page_title' => $row['page_title'], 'page_namespace' => $row['page_namespace']);
			}
		}
		$statement->closeCursor();
		$statement = null;
		
		if($catdepth > 0){
			foreach($subcategories as $pageid => $subcategory){
				$pages = $pages + $this->scan_cat($wikiDB, $subcategory, $catdepth-1);
			}
		}
		
		return $pages;
	}
	
	private function makeForm(){
		$this->userMsg .= '			<p>This tool is still under development and might not always work properly.</p>' . N;
		$this->userMsg .= '			<form action="' . $_SERVER['SCRIPT_NAME'] . '" method="post"><table class="form"><tbody>';
		$this->userMsg .= '<tr><td><label for="wiki">Project:</label></td><td><select name="projectlang" id="projectlang" style="width:65px;">';
		foreach($this->available_languages as $projectlang){
			$this->userMsg .= '<option value="' . $projectlang . '"';
			if($projectlang === $this->projectlang){
				$this->userMsg .= ' selected="selected"';
			}
			$this->userMsg .= '>' . $projectlang . '</option>';
		}
		$this->userMsg .= '</select>.<select name="project" id="project" style="width:105px;">';
		foreach($this->available_projects as $project => $short_project){
			$this->userMsg .= '<option value="' . $project . '"';
			if($project === $this->project){
				$this->userMsg .= ' selected="selected"';
			}
			$this->userMsg .= '>' . $project . '</option>';
		}
		$this->userMsg .= '</select>.org</td><td></td></tr>';
		$this->userMsg .= '<tr><td><label for="category">Category:</label></td><td><input type="text" name="category" id="category"' . ($this->category !== null?' value="' . str_replace('_', ' ', $this->category) . '"':'') . '></td><td>without namespace prefix</td></tr>';
		$this->userMsg .= '<tr><td><label for="catdepth">Cat search depth:</label></td><td><input type="text" name="catdepth" id="catdepth" value="' . $this->catdepth . '"></td><td></td></tr>';
		$this->userMsg .= '<tr><td><label for="format">Output format:</label></td><td><select name="format" id="format">';
		foreach($this->output_formats as $format){
			$this->userMsg .= '<option value="' . $format . '"';
			if($format === $this->format){
				$this->userMsg .= ' selected="selected"';
			}
			$this->userMsg .= '>' . $format . '</option>';
		}
		$this->userMsg .= '</select></td><td></td></tr>';
		$this->userMsg .= '<tr><td><label for="detaileduser">Detailed for user:</label></td><td><input type="text" name="detaileduser" id="detaileduser"' . ($this->detailed_user !== $this->default_detailed_user?' value="' . str_replace('_', ' ', $this->detailed_user) . '"':'') . '></td><td>leave empty for full results; <span style="color:red;">not yet fully developed</span></td></tr>';
		$this->userMsg .= '<tr><td>Page sortkey:</td><td colspan="2">';

		$output_components = array();
		foreach($this->sortdirs as $dir => $text){
			$output_components[] = '<label for="pagesortdir-' . $dir . '">' . $text . '</label> <input type="radio" name="pagesortdir" id="pagesortdir-' . $dir . '" value="' . $dir . '"' . ($this->pagesortdir === $dir?' checked="checked"':'') . '>';
		}
		$this->userMsg .= implode(' — ', $output_components);
		
		$this->userMsg .= '<br>';

		$output_components = array();
		foreach($this->pages_sortkeys as $sortkey => $text){
			$output_components[] = '<label for="pagesortkey-' . $sortkey . '">' . $text . '</label> <input type="radio" name="pagesortkey" id="pagesortkey-' . $sortkey . '" value="' . $sortkey . '"' . ($this->pagesortkey === $sortkey?' checked="checked"':'') . '>';
		}
		$this->userMsg .= implode(' — ', $output_components);
		
		$this->userMsg .= '</td></tr>';
		$this->userMsg .= '<tr><td>User sortkey:</td><td colspan="2">';
		
		$output_components = array();
		foreach($this->sortdirs as $dir => $text){
			 $output_components[] = '<label for="usersortdir-' . $dir . '">' . $text . '</label> <input type="radio" name="usersortdir" id="usersortdir-' . $dir . '" value="' . $dir . '"' . ($this->usersortdir === $dir?' checked="checked"':'') . '>';
		}
		$this->userMsg .= implode(' — ', $output_components);
		
		$this->userMsg .= '<br>';

		$output_components = array();
		foreach($this->users_sortkeys as $sortkey => $text){
			$output_components[] = '<label for="usersortkey-' . $sortkey . '">' . $text . '</label> <input type="radio" name="usersortkey" id="usersortkey-' . $sortkey . '" value="' . $sortkey . '"' . ($this->usersortkey === $sortkey?' checked="checked"':'') . '>';
		}
		$this->userMsg .= implode(' — ', $output_components);
		
		$this->userMsg .= '</td></tr>';
		$this->userMsg .= '<tr><td><label for="limitusers">Limit user list:</label></td><td><input type="text" name="limitusers" id="limitusers" value="' . $this->limit_display_users . '"></td><td>number of entries, strongly recommended</td></tr>';
		
		$this->userMsg .= '<tr><td><input type="hidden" name="run" value="1">';
		$this->userMsg .= '<input type="hidden" name="tool" value="' . $this->toolName . '">';
		$this->userMsg .= '</td><td><input type="submit" value="run"></td><td>';
		$this->userMsg .= '</td></tr></tbody></table></form>' . N;
		$this->userMsg .= '<p>Please be aware that this is a heavy operation that needs time to be processed and results quite a large output of several MB html source. The size of this operation depends on the number of articles selected by the catscan above. If the tool result cannot be processed within ' . ini_get('max_execution_time') . 's or if it requests more than ' . ini_get('memory_limit') . ' of server memory, the script will terminate without results (error: “400 – Bad Request”) and you could try smaller selections.</p>' . N;
		$this->userMsg .= '<p>This tool uses the replica databases available on <a href="//tools.wmflabs.org/">Tool Labs</a> for evaluation of the request. Edits in Wikipedia are typically available in the replicas after some seconds. Results are based on the current categorization, thus deleted articles and formerly categorized articles that are no longer part of the selected category tree are not included here.</p>' . N;
	}
	
	public static function initDatabase($database){
	}
}

?>