<?php

class Example extends Tool {
	
	protected $toolName = 'Example';
	protected $hasToolDatabase = true;
	
	public $output_formats = array('json-html', 'json', 'html');	// first element is default choice
	
	public function execute(){
		$this->helloWorld();
		
		$this->userMsg .= '<p>This is a test tool. No contents here.</p>' . N;
		
/*		$this->userMsg .= '<form action="' . $_SERVER['SCRIPT_NAME'] . '" method="post">';
		$this->userMsg .= '<select name="format">';
		foreach($this->output_formats as $format){
			$this->userMsg .= '<option value="' . $format . '"';
			if($format === $this->format){
				$this->userMsg .= ' selected="selected"';
			}
			$this->userMsg .= '>' . $format . '</option>';
		}
		$this->userMsg .= '</select>';
		$this->userMsg .= '<input type="hidden" name="run" value="1">';
		$this->userMsg .= '<input type="hidden" name="tool" value="' . $this->toolName . '">';
		$this->userMsg .= '<input type="submit" value="run"></form>' . N;
		
		$dewiki = new ReplicaDatabase('dewiki');
		if($dewiki->getToolDBName() !== null){
			$this->userMsg .= '<p>Connected to <em>dewiki</em> replica.</p>' . N;
			$statement = $dewiki->prepare('SHOW TABLES');
			$statement->execute();
			$this->userMsg .= '<table>' . N;
			while($row = $statement->fetch()){
				$this->userMsg .= '<tr><td>' . $row[0] . '</td></tr>' . N;
			}
			$this->userMsg .= '</table>' . N;
			$statement->closeCursor();
			$statement=null;
			
			$statement2 = $dewiki->prepare('SELECT * FROM category LIMIT 100');
			$statement2->execute();
			$this->userMsg .= '<table>' . N;
			while($row = $statement2->fetch(PDO::FETCH_ASSOC)){
				$this->userMsg .= '<tr><td>' . implode(' --- ', array_keys($row)) . '</td><td>' . implode(' --- ', $row) . '</td></tr>' . N;
			}
			$this->userMsg .= '</table>' . N;
			$statement2->closeCursor();
			$statement2=null;
			
			$dewiki = null;
			$this->userMsg .= '<p>Disconnected from <em>dewiki</em> replica.</p>' . N;
		}
		
		if($this->run === '1'){	// only if tool should output a result
			$this->toolResult = array('testmethod' => 1, 'data' => array('en' => 'result', 'de' => 'Ergebnis'));
			$this->userMsg .= '<hr>' . N;
			
			if($this->toolDB !== null){
				$this->userMsg .= '<p>Try to use Tool Database <code>' . $this->toolDB->getToolDBName() . '</code></p>' . N;
				$query = 'SELECT * FROM test ORDER BY `dummy` DESC';
				$statement = $this->toolDB->prepare($query);
				$statement->execute();
				$this->userMsg .= '<ul>' . N;
				while($row = $statement->fetch(PDO::FETCH_ASSOC)){
					$this->userMsg .= '<li>' . $row['ID'] . ' : ' . $row['dummy'] . '</li>' . N;
				}
				$this->userMsg .= '</ul>' . N;
				$statement->closeCursor();
				
				$this->userMsg .= '<hr>' . N;
				$this->userMsg .= '<p>Now try to add something to the database.</p>' . N;
				$query = 'INSERT INTO test (dummy) VALUES (:dummy)';
				$statement2 = $this->toolDB->prepare($query);
				$statement2->bindValue(':dummy', '„Blah“ ' . date('s', time()));
				$statement2->execute();
				$statement2->closeCursor();
			}
			else {
				$this->userMsg .= '<p>We don’t have a tool database connection.</p>' . N;
			}
		}
*/		return $this->userMsg;
	}
	
	public function format_result(){
		if(count($this->toolResult) > 0){
			if(substr($this->format, 0, 4) === 'json'){
				$this->formattedToolResult = json_encode($this->toolResult);
			}
			else if(substr($this->format, 0, 4) === 'html'){
				if($this->toolResult['testmethod'] === 1){
					$this->formattedToolResult = '<p>This is a testmethod.</p>' . N;
				}
				$this->formattedToolResult .= '<table>';
				foreach($this->toolResult['data'] as $key => $value){
					$this->formattedToolResult .= '<tr><td>' . $key . '</td><td>' . $value . '</td></tr>';
				}
				$this->formattedToolResult .= '</table>' . N;
			}
		}
		else {	// empty result
			$this->formattedToolResult = null;
		}
		return $this->formattedToolResult;
	}
	
	public static function initDatabase($database){
		$sql = array();
		
		$sql[] = 'CREATE DATABASE IF NOT EXISTS `' . $database . '` DEFAULT CHARACTER SET ' . Database::$db_conf['charset'] . ' COLLATE ' . Database::$db_conf['collation'] . ';';
		$sql[] = 'USE `' . $database . '`;';
		
		$table_name = 'test';
		
		$sql[] =	'CREATE TABLE IF NOT EXISTS `' . $table_name . '` (' .
					'`ID` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY, ' .
					'`dummy` varchar(16) COLLATE ' . Database::$db_conf['collation'] . ' NOT NULL' .
					') ENGINE=' . Database::$db_conf['engine'] . ' AUTO_INCREMENT=1 DEFAULT CHARSET=' . Database::$db_conf['charset'] . ' COLLATE=' . Database::$db_conf['collation'] . ';';
		
		return $sql;
	}
}

?>