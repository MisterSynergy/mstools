<?php

abstract class Tool {
	
	protected $toolName = '';
	protected $hasToolDatabase = false;
	protected $toolDB = null;
	
	protected $run = DEFAULT_AUTORUN;
	protected $format = DEFAULT_OUTPUT_FORMAT;
	
	protected $userMsg = '';				// HTML format inside body tag
	protected $toolResult = array();		// array
	protected $formattedToolResult = '';	// formatted string of array according to $this->format
	
	public function __construct($inputdata){
		if(isset($inputdata['run'])){
			$this->run = $inputdata['run'];
		}
		
		if(isset($inputdata['format'])){
			if(in_array($inputdata['format'], $this->output_formats)){
				$this->format = $inputdata['format'];
			}
			else {
				$this->format = $this->output_formats[0];
			}
		}		
		
		if($this->hasToolDatabase === true){
			$this->toolDB = new ToolDatabase(strtolower($this->toolName));
		}
	}
	
	public function __destruct(){
		$this->toolDB = null;
	}
	
	abstract public function execute();
	
	public function format_result(){	// default output
		if(count($this->toolResult) > 0){
			$this->formattedToolResult = json_encode($this->toolResult);
		}
		else {
			$this->formattedToolResult = null;
		}
		return $this->formattedToolResult;
	}
	
	public function helloWorld(){
		$this->userMsg .= '<p>An instance of <em>' . $this->toolName . '</em> says “Hello World!”</p>' . N;
		$this->userMsg .= '<p>Important runtime parameters are: <code>run=' . $this->run . '</code> and <code>format=' . $this->format . '</code>.</p>' . N;
	}
	
	public function curl($url){
		$ch = curl_init();
		curl_setopt_array($ch, array(
			CURLOPT_URL => $url,
			CURLOPT_HEADER => 0,
			CURLOPT_RETURNTRANSFER => true,
			//CURLOPT_USERAGENT => 'mstools/1.0 (maintained by mister.synergy@yahoo.com; +https://' . PRODSERV . '/mstools/index.php?tool=' . $this->toolName . ')',
			CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/42.0',
			CURLOPT_SSL_VERIFYHOST => 0,
			CURLOPT_SSL_VERIFYPEER => 0
		));
		$data = curl_exec($ch);
		$status = curl_getinfo($ch);
		curl_close($ch);
		return array('status' => $status, 'data' => $data);
	}
	
	public function obj_out($obj){
		echo '<!--' . N;
		print_r($obj);
		echo '-->' . N;
	}

	function purge($wdq){
		$search  = array('[',    ']',    ':',    ' ',    ',');
		$replace = array('%%5B', '%%5D', '%%3A', '%%20', '%%2C');
		return str_replace($search, $replace, $wdq);
	}
	
	
	public function ns($ns, $lang = 'en'){
		if(false === in_array($lang, array('en', 'de'))){
			$lang = 'en';
		}
		
		switch($lang){
			case 'en':
				$namespaces = array(
					'0' => '',
					'1' => 'Talk',
					'2' => 'User',
					'3' => 'User_talk',
					'4' => 'Wikipedia',
					'5' => 'Wikipedia_talk',
					'6' => 'File',
					'7' => 'File_talk',
					'8' => 'MediaWiki',
					'9' => 'MediaWiki_talk',
					'10' => 'Template',
					'11' => 'Template_talk',
					'12' => 'Help',
					'13' => 'Help_talk',
					'14' => 'Category',
					'15' => 'Category_talk',
					'100' => 'Portal',
					'101' => 'Portal_talk',
					'118' => 'Draft',
					'119' => 'Draft_talk'
				);
				break;
			case 'de':
				$namespaces = array(
					'0' => '',
					'1' => 'Diskussion',
					'2' => 'Benutzer',
					'3' => 'Benutzer_Diskussion',
					'4' => 'Wikipedia',
					'5' => 'Wikipedia_Diskussion',
					'6' => 'Datei',
					'7' => 'Datei_Diskussion',
					'8' => 'MediaWiki',
					'9' => 'MediaWiki_Diskussion',
					'10' => 'Vorlage',
					'11' => 'Vorlage_Diskussion',
					'12' => 'Hilfe',
					'13' => 'Hilfe_Diskussion',
					'14' => 'Kategorie',
					'15' => 'Kategorie_Diskussion',
					'100' => 'Portal',
					'101' => 'Portal_Diskussion'
				);
				break;
		}
		return $namespaces[$ns];
	}
}

?>