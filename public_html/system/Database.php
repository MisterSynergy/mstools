<?php

abstract class Database extends PDO {

//	https://wikitech.wikimedia.org/wiki/Help:Tool_Labs/Database
//	https://quarry.wmflabs.org/query/4031	(Available databases)
//	https://dbtree.wikimedia.org/
//	https://wikitech.wikimedia.org/wiki/Help:MySQL_queries
	
	protected $database = null;
	protected $host = null;
	
	private static $db_cred = array(
		PRODSERV => '/data/project/mstools/replica.my.cnf',
		TESTSERV => '../../mstools_meta/replica.my.cnf'
	);
	
	protected static $db_options = array(
	);
	
	public static $db_conf = array(
		'driver' => 'mysql',
		'charset' => 'utf8mb4',
		'collation' => 'utf8mb4_bin',
		'engine' => 'InnoDB'
	);
	
	public static $db_select = array(
		PRODSERV => array(
			'tools_db_name' => '%1$s__%2$s',
			'tools_host_name' => 'tools.db.svc.eqiad.wmflabs',
			'replica_db_name' => '%1$s_p',
			'replica_host_name' => '%1$s.analytics.db.svc.wikimedia.cloud'
		),
		TESTSERV => array(
			'tools_db_name' => 'mstools__%2$s',
			'tools_host_name' => 'localhost',
			'replica_db_name' => null,
			'replica_host_name' => null
		)
	);
	
//	Nice to have: MySQL-Fehlerbehandlung mit mysql_error() oder PDO-Funktionalität
	
	public function __construct($database, $host){
		$this->database = $database;
		$this->host = $host;
		
		$credentials = $this->getCredentials(true, true);
		try {
			$dsn = sprintf('%1$s:host=%2$s;charset=%3$s', self::$db_conf['driver'], $host, self::$db_conf['charset']);
			parent::__construct($dsn, $credentials['user'], $credentials['password'], self::$db_options);
			unset($credentials);
			
			if(false === $this->exec('USE ' . $database)){
				if(substr($this->host, -7, 7) !== '.labsdb'){	// create database only in tool space, not in replicas
					$class = ucfirst(strtolower(substr($database, strpos($database, '__')+2)));
					$queries = $class::initDatabase($database);	// array of queries
					foreach($queries as $query){
						$this->exec($query);
					}
				}
				else {
					echo '<!-- cannot open database ' . $database . ' -->' . N;
				}
			}
		} catch (PDOException $e){
			echo '<!-- Connection failed: ' . $e->getMessage() . ' -->';
			return false;
		}
	}
	
	final public function __destruct(){
		// nothing to do
	}
	
	public function getToolDBName(){
		if($_SERVER['SERVER_NAME'] !== TESTSERV){
			return '/hidden/';
		}
		else {
			return $this->database;
		}
	}
	
	private function getCredentials($user = true, $password = false){
		$credentials = parse_ini_file(self::$db_cred[$_SERVER['SERVER_NAME']]);
		if($user === false){
			$credentials['user'] = null;
		}
		if($password === false){
			$credentials['password'] = null;
		}
		return $credentials;
	}
}

?>