<?php

class ReplicaDatabase extends Database {

	public function __construct($database){
		if($_SERVER['SERVER_NAME'] === PRODSERV){ // no replicas on localhost testing servers
			parent::__construct(sprintf(self::$db_select[$_SERVER['SERVER_NAME']]['replica_db_name'], $database), sprintf(self::$db_select[$_SERVER['SERVER_NAME']]['replica_host_name'], $database));
		}
	}

}

?>