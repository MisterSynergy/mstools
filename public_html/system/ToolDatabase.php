<?php

class ToolDatabase extends Database {
	
	public function __construct($database){
		$credentials = $this->getCredentials();
		parent::__construct(sprintf(self::$db_select[$_SERVER['SERVER_NAME']]['tools_db_name'], $credentials['user'], $database), self::$db_select[$_SERVER['SERVER_NAME']]['tools_host_name']);
	}
	
	public function initDatabase($db_name, $sql = ''){
		// to be implemented
	}
}

?>