<?php

ini_set('max_execution_time', 300);
ini_set('memory_limit', '256M');

// 1) define basic inputs
define('N', "\n");
define('T', "\t");
define('TOOLDIR', './tools/');
define('SYSDIR', './system/');
define('PRODSERV', 'mstools.toolforge.org');
define('TESTSERV', 'wikiprojects');
define('DEFAULT_AUTORUN', '0');
$output_formats = array('html', 'json', 'json-html', 'txt', 'txt-html', 'wiki', 'wiki-html', 'xml', 'xml-html');
define('DEFAULT_OUTPUT_FORMAT', $output_formats[0]);

// 2) basic configuration
if(true || $_SERVER['SERVER_NAME'] === TESTSERV)	error_reporting(E_ALL | E_STRICT);
else 												error_reporting(0);

if(false) exit('page offline');	// emergency off

// 3) initialize user inputs ($_POST preferred over $_GET, but both work)
if(isset($_POST) && count($_POST) > 0){
	$inputdata = &$_POST;
}
else if(isset($_GET)){
	$inputdata = &$_GET;
}
else {
	$inputdata = null;
}

$tool = isset($inputdata['tool'])?ucfirst(strtolower($inputdata['tool'])):'Index';

if(isset($inputdata['format']) && in_array($inputdata['format'], $output_formats)){
	$inputdata['format'] = strtolower($inputdata['format']);
}
else {
	$inputdata['format'] = DEFAULT_OUTPUT_FORMAT;
}

if(!isset($inputdata['run']) || false === in_array($inputdata['run'], array('1', '0'))){
	$inputdata['run'] = DEFAULT_AUTORUN;
}

// 4) include PHP files
$inc_files = array('Tool', 'Database', 'ReplicaDatabase', 'ToolDatabase');	// Files to include always
foreach($inc_files as $file){
	include_once(SYSDIR . $file . '.php');
}
$available_tools = array();
if(is_dir(TOOLDIR)){	// look at __autoload()	https://secure.php.net/manual/de/language.oop5.autoload.php
	if(false !== ($dh = opendir(TOOLDIR))){
		while(false !== ($file = readdir($dh))) {
			if($file !== '.' && $file !== '..' && preg_match('/(.*).php$/', $file, $matches)){
				$available_tools[] = $matches[1];
				if(strtolower($tool) === strtolower($matches[1])){	// include only the selected tool
					$tool = $matches[1];
					include_once(TOOLDIR . $file);
				}
			}
		}
		closedir($dh);
	}
}

// 5) operate tool and obtain formatted tool result
$toolUserMessage = null;
$formattedToolResult = null;
if(in_array($tool, $available_tools)){
	$t = new $tool($inputdata); // initializes
	$toolUserMessage = $t->execute();	// put inputs to html forms and provide a submit (run) button (run=0) or executes directly (run=1)
	$formattedToolResult = $t->format_result();
}

// 6) output to user
output($inputdata['format'], $formattedToolResult, $toolUserMessage, $available_tools, $tool, $inputdata['run']);

function output($format, $formattedToolResult, $toolUserMessage, $available_tools, $selected_tool, $run){	
	if(substr($format, -4, 4) === 'html'){
		echo html_header($selected_tool);
		echo html_navi($available_tools, $selected_tool);
		if($formattedToolResult === null && $toolUserMessage === null){
			echo index_page();
		}
		else {
			echo '		<section id="input" class="content">' . N;
			echo '			<h2>Input</h2>' . N;
			echo $toolUserMessage;
			echo '		</section>' . N;
			if($run === '1' && $formattedToolResult !== null){
				echo '		<section id="results" class="content">' . N;
				echo '			<h2>Tool results</h2>' . N;
				if($format !== 'html') echo '			<pre>';
				echo $formattedToolResult;
				if($format !== 'html') echo '</pre>' . N;
				echo '		</section>' . N;
			}
			else if($run === '1'){
				echo '		<section id="noresult" class="content">' . N;
				echo '			<p>No tool result.</p>' . N;
				echo '		</section>' . N;
			} 
		}
		echo html_footer();
	}
	else {
		echo $formattedToolResult;
	}
}

function html_header($selected_tool){
	$header  = '<!DOCTYPE html>' . N;
	$header .= '<html lang="en">' . N;
	$header .= '	<head>' . N;
	$header .= '		<meta charset="UTF-8">' . N;
	$header .= '		<meta name="author" content="MisterSynergy">' . N;
	$header .= '		<meta http-equiv="reply-to" content="mister.synergy@yahoo.com">' . N;
	$header .= '		<meta name="description" content="mstools by MisterSynergy">' . N;
	$header .= '		<meta name="keywords" lang="en" content="">' . N;
	$header .= '		<meta name="robots" content="noindex, nofollow">' . N;
	$header .= '		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">' . N;
	$header .= '		<link href="./resources/favicon.ico" rel="shortcut icon" type="image/x-icon">' . N;
	$header .= '		<link href="./resources/mstools.css" rel="stylesheet" type="text/css">' . N;
	$header .= '		<title>' . ($selected_tool !== 'Index'?$selected_tool . ' • ':'') . 'mstools by MisterSynergy</title>' . N;
	$header .= '	</head>' . N;
	$header .= '	<body>' . N;
	return $header;
}

function html_navi($available_tools, $selected_tool){
	$nav  = '		<nav>' . N;
	$nav .= '			<div class="left"><a href="' . $_SERVER['SCRIPT_NAME'] . '" name="top">Index</a>' . ($selected_tool !== 'Index'?' • Tool: ' . $selected_tool:'') . '</div>' . N;
	$nav .= '			<div class="right"><a href="//de.wikipedia.org/wiki/Benutzer_Diskussion:MisterSynergy">Contact</a> • <a href="#">Help</a></div>' . N;
	$nav .= '			<form class="center" action="' . $_SERVER['SCRIPT_NAME'] . '" method="post"><label for="tool">Tool:</label> <select id="tool" name="tool">';
	foreach($available_tools as $available_tool){
		$nav .= '<option id="' . $available_tool . '" value="' . $available_tool . '"' . (strtolower($selected_tool) === strtolower($available_tool)?' selected="selected"':'') . '>' . $available_tool . '</option>';
	}
	$nav .= '</select> <input type="submit" value="select"></form>' . N;
	$nav .= '		</nav>' . N;
	return $nav;
}

function html_footer(){
	$footer  = '		<footer>' . ($_SERVER['SERVER_NAME'] === TESTSERV?'<span style="color:#F77; font-weight:bold;">Testserver</span> (<a href="https://' . PRODSERV . '/mstools/index.php">production server</a>) • ':'') . '“mstools” has beta status • <a href="https://wikitech.wikimedia.org/wiki/Tool:Mstools">info page</a> on <a href="https://wikitech.wikimedia.org/">wikitech</a> • last modified: ' . date('D, j M Y G:i:s T', filemtime($_SERVER['SCRIPT_FILENAME'])) . '</footer>' . N;
	$footer .= '	</body>' . N;
	$footer .= '</html>';
	return $footer;
}

function index_page(){
	$index_page  = '		<section id="index" class="content">' . N;
	$index_page .= '			<h1>“mstools” by MisterSynergy</h1>' . N;
	$index_page .= '			<h2>Preamble: beta status</h2>' . N;
	$index_page .= '			<p>This is a relatively fresh tool page on Wikimedia’s <a href="//' . PRODSERV . '/">Tool Labs</a> server.</p>' . N;
	$index_page .= '			<p>In the past years, I have regulary used a bunch of different self-developed scripts to make many of my edits actually possible. I ran these scripts on a local webserver, which is rather inefficient for several reasons. Since their functionality might be interesting for other users as well, I now decided to move to Wikimedia’s Tool Labs server instead.</p>' . N;
	$index_page .= '			<p>However, I expect that most of my scripts need an entire re-write, thus it will take some time until something useful is available here. I am also new here on Tool Labs, thus there are a couple of things to learn in the beginning. Stay tuned! —MisterSynergy</p>' . N;
	$index_page .= '			<h2>Available tools</h2>' . N;
	$index_page .= '			<dl>' . N;
	$index_page .= '				<dt><a href="' . $_SERVER['SCRIPT_NAME'] . '?tool=RedirFinder">RedirFinder</a></dt>' . N;
	$index_page .= '				<dd>Based on a given category in a Wikimedia project, you can list all redirects to articles within that category, including subcategories. This lets you evaluate the usage of redirects with a given topical area.</dd>' . N;
	$index_page .= '				<dt><a href="' . $_SERVER['SCRIPT_NAME'] . '?tool=MainAuthors">MainAuthors</a></dt>' . N;
	$index_page .= '				<dd>If you ever wanted to know which authors have been being active in a given topical area, this is your tool. You will need to give a root category, this tool lists all involved users according to their amount of contributions.</dd>' . N;
	$index_page .= '			</dl>' . N;
	$index_page .= '			<h2>Contact</h2>' . N;
	$index_page .= '			<p>I am known as <a href="//de.wikipedia.org/wiki/Benutzer:MisterSynergy">MisterSynergy</a> at German Wikipedia (and some other Wikimedia projects). Feel free to use German or English tongue to get in contact with me on my talk page.</p>' . N;
	$index_page .= '		</section>' . N;
	return $index_page;
}

?>